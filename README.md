# Tutorials for Health Data Science

This project contains tutorials for Health Data Science. They rely on synthetic datasets or open datasets related to health. Most of the tutorials do not require the installation of any software. They propose Notebooks that can run on [Jupyter](https://jupyter.org/).

## Tutorials

- Basics of programming (in development, see the branch dev_basics)
- First program (in development, see the branch dev_first_program)
- Data loading (not yet in development)
- Overview of data management
- Advanced data management (in development, see the branch dev_advanced_data_management)
- Graphics (in development, see the branch dev_graphics)
- Shiny (in development, see the branch dev_shiny)
- Text mining (in development, see the branch dev_text_mining)
- Cartography (in development, see the branch dev_carto)
- Sankey Diagram