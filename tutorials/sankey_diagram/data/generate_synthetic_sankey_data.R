######################################################################################
#
# This script generates the raw dataset required to produce a Sankey diagram
#
# The dataset include the following columns :
# - patient_id
# - step : the label of the step
# - start_date
# - end_date
#
# Antoine Lamer
#
# 22/08/16
#
######################################################################################

# Loading packages
# ------------------------------------------------------------------------------------
library(dplyr)
library(tidyr)

# Global variables
# ------------------------------------------------------------------------------------
patients_number = 100
probs_step_2 = c(0.2, 0.4, 0.4)
probs_step_3 = c(0.6, 0.4)

# Generate step 1 for 100 patients
# ------------------------------------------------------------------------------------
start_date_step_1 = sample(seq(as.Date('2015/01/01'), as.Date('2017/12/31'), by="day"), 
                      patients_number)
delay_step_1 = round(runif(patients_number, min=0, max=2))
end_date_step_1 = start_date_step_1 + delay_step_1

# Generate step 2
# ------------------------------------------------------------------------------------

step_2 = sample(c("B", "C", "D"), patients_number, prob = probs_step_2, replace = TRUE)
delay_step_2 = round(runif(patients_number, min = 1, max = 10))
end_date_step_2 = end_date_step_1 + delay_step_2

# Generate step 3
# ------------------------------------------------------------------------------------

step_3 = sample(c("E", "F"), patients_number, prob = probs_step_3, replace = TRUE)
delay_step_3 = round(runif(patients_number, min = 1, max = 5))
end_date_step_3 = end_date_step_2 + delay_step_3

# Gather the 3 steps in a wide dataframe
# ------------------------------------------------------------------------------------

data_sankey_wide = data.frame(patient_id = rep(1:patients_number), 
                              step_1 = "A", 
                              start_date_step_1 = start_date_step_1,
                              end_date_step_1 = end_date_step_1,
                              step_2 = step_2,
                              start_date_step_2 = end_date_step_1,
                              end_date_step_2 = end_date_step_2,
                              step_3 = step_3,
                              start_date_step_3 = end_date_step_2,
                              end_date_step_3 = end_date_step_3)

# Transform into a long dataframe
# ------------------------------------------------------------------------------------

step_1 = data_sankey_wide %>% 
  select(patient_id, step = step_1, start_date = start_date_step_1, end_date = end_date_step_1)

step_2 = data_sankey_wide %>% 
  select(patient_id, step = step_2, start_date = start_date_step_2, end_date = end_date_step_2)

step_3 = data_sankey_wide %>% 
  select(patient_id, step = step_3, start_date = start_date_step_3, end_date = end_date_step_3) 

data_sankey = rbind(step_1, step_2, step_3) %>% 
  arrange(patient_id, start_date, end_date)

# Export the dataframe
# ------------------------------------------------------------------------------------

data_sankey %>%
  write.csv2("data_sankey_100_patients.csv", row.names = FALSE)
