
# Markdown & HTML

<div class="panel panel-info">
**Note**
{: .panel-heading}
<div class="panel-body">

NOTE DESCRIPTION

</div>
</div>

My important paragraph.
{: .alert .alert-info}

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

## TODO 

- [ ] graphique en barres avec seulement la modalité Oui / 1